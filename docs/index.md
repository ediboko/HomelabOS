# HomelabOS

Welcome to HomelabOS! Your very own offline-first open-source data-center!

HomelabOS is a collection of various separate services. You can find more information about each in the menu on the left.

## [Installation](/setup/installation)
## [Getting Started](/setup/gettingstarted)

## Getting Help

If you are having problems you can:

  - [File an issue on GitLab](https://gitlab.com/NickBusey/HomelabOS/issues).
  - [Ask a question on HomelabOS Reddit](https://www.reddit.com/r/HomelabOS/)
  - [Ask a question HomelabOS Zulip Chat](https://homelabos.zulipchat.com/)

## Available Software

### Categories

  - [Analytics](#analytics)
  - [Automation](#automation)
  - [Blogging Platforms](#blogging-platforms)
  - [Calendaring and Contacts Management](#calendaring-and-contacts-management)
  - [Chat](#chat)
  - [Document Management](#document-management)
  - [E-books](#e-books)
  - [Email](#email)
  - [Federated Identity/Authentication](#federated-identityauthentication)
  - [Feed Readers](#feed-readers)
  - [File Sharing and Synchronization](#file-sharing-and-synchronization)
  - [Gateways and terminal sharing](#gateways-and-terminal-sharing)
  - [Media Streaming](#media-streaming)
  - [Misc/Other](#miscother)
  - [Money, Budgeting and Management](#money-budgeting-and-management)
  - [Monitoring](#monitoring)
  - [Note-taking and Editors](#note-taking-and-editors)
  - [Password Managers](#password-managers)
  - [Personal Dashboards](#personal-dashboards)
  - [Photo and Video Galleries](#photo-and-video-galleries)
  - [Read it Later Lists](#read-it-later-lists)
  - [Software Development](#software-development)
  - [Task management/To-do lists](#task-managementto-do-lists)
  - [VPN](#vpn)
  - [Web servers](#web-servers)
  - [Wikis](#wikis)

### Analytics

- [Matomo](https://matomo.org/) - Web analytics

### Automation

- [Home Assistant](https://www.home-assistant.io/) - Home Automation
- [HomeBridge](https://homebridge.io/) - HomeKit support for the impatient
- [Kibitzr](https://kibitzr.github.io/) - IFTTT replacement

### Blogging Platforms

- [Ghost](https://ghost.org/) - Ghost is a platform for building and running a modern online publication

### Calendaring and Contacts Management

- [NextCloud](https://nextcloud.com/) - Private Cloud Storage, Calendar, Contacts, etc.

### Chat

- [The Lounge](https://thelounge.chat/) - Always-on IRC client (IRC bouncer)
- [Zulip](https://github.com/zulip/zulip) - Threaded chat software

### Document Management

- [Mayan EDMS](https://gitlab.com/mayan-edms/mayan-edms) - Free Open Source DMS (document management system).
- [Paperless](https://github.com/danielquinn/paperless) - Document management

### E-Books

- [Calibre](https://calibre-ebook.com) - Calibre and Calibre Web - Complete ebook library management.

### Email

- [Mailserver](https://github.com/hardware/mailserver/) - Fully featured mail server
- [Mailu](https://mailu.io/1.7/general.html) - is a simple yet full-featured mail server as a set of Docker images.

### Federated Identity/Authentication

- [Authelia](https://www.authelia.com) - Authelia is an open-source full-featured authentication server available on Github
- [PhpLDAPadmin](https://github.com/osixia/docker-phpLDAPadmin) - LDAP management interface

### Feed Readers

- [FreshRSS](https://freshrss.org) - A free, self-hostable aggregator
- [Miniflux](https://miniflux.app/) - Miniflux is a minimalist and opinionated feed reader.

### File Sharing and Synchronization

- [Jackett](https://github.com/Jackett/Jackett) - API Support for your favorite torrent trackers (helps Sonarr and Radarr)
- [Lidarr](https://lidarr.audio) - Sonarr but for Music.
- [Minio](https://minio.io/) - S3 hosting
- [Mylar](https://github.com/evilhero/mylar) - An automated Comic Book manager
- [Ombi](https://ombi.io) - Ombi is a self-hosted web application that automatically gives your shared Plex or Emby users the ability to request content by themselves!
- [Radarr](https://radarr.video/) - Automated movie downloading
- [SickChill](https://sickchill.github.io/) - SickChill is an automatic Video Library Manager for TV Shows.
- [Sonarr](https://sonarr.tv/) - Automated TV downloading
- [Syncthing](https://syncthing.net/) - File syncing software
- [Transmission](https://transmissionbt.com/) - BitTorrent client

### Gateways and terminal sharing

- [Guacamole](https://guacamole.apache.org) - a clientless remote desktop gateway
- [WebVirtMg](https://github.com/retspen/webvirtmgr) is a complete Kernel Virtual Machine (KVM) hypervisor manager.

### Media Streaming

- [Airsonic](https://airsonic.github.io/) - Airsonic is a free, web-based media streamer, providing ubiquitous access to your music.
- [Beets](https://beets.io) - Beets is the media library management system for obsessive-compulsive music geeks.
- [Emby](https://emby.media/) - Personal Media Server
- [Jellyfin](https://github.com/jellyfin/jellyfin) - The Free Software Media System
- [MStream](https://www.mstream.io) - All your music, everywhere you go.
- [Plex](https://www.plex.tv/) - Personal Media Server

### Misc/Other

- [Darksky](http://darksky.net/) - Local weather reported via [darksky-influxdb](https://github.com/ErwinSteffens/darksky-influxdb)
- [Dasher](https://github.com/maddox/dasher) - Amazon Dash button support
- [Documentation](https://nickbusey.gitlab.io/HomelabOS/) - Offline, searchable documentation via [MkDocs](https://www.mkdocs.org/)
- [Grocy](https://grocy.info) - ERP beyond your fridge - grocy is a web-based self-hosted groceries & household management solution for your home
- [Grownetics](https://grownetics.co) - Open source environmental mapping with plant management and tracking
- [Mashio](https://gitlab.com/NickBusey/mashio) - Home brewery management software
- [Minecraft](https://hub.docker.com/r/itzg/minecraft-server) Minecraft server with select-able version 
- [Monica](https://www.monicahq.com/) - Contact / relationship manager
- [Pi-hole](https://pi-hole.net/) - Ad blocking
- [Poli](https://github.com/shzlw/poli) - An easy-to-use BI server built for SQL lovers. Power data analysis in SQL and gain faster business insights.
- [Portainer](https://www.portainer.io/) - Easy Docker management
- [PrivateBin](https://privatebin.info) PrivateBin is a minimalist, open source online pastebin where the server has zero knowledge of pasted data.
- [Searx](https://github.com/asciimoo/searx/) - A privacy-respecting, hackable metasearch engine.
- [Ubooquity](https://vaemendis.net/ubooquity/) - Ubooquity is a free home server for your comics and ebooks library
- [Watchtower](https://containrrr.github.io/watchtower/) - A process for automating Docker container base image updates

### Money, Budgeting and Management

- [Firefly III](https://firefly-iii.org/) - Money management budgeting app

### Monitoring

- [Grafana](https://grafana.com/) - Pretty graphs
- [HealthChecks](https://HealthChecks.io) - A Cron Monitoring Tool written in Python & Django https://healthchecks.io
- [InfluxDB](https://www.influxdata.com/time-series-platform/influxdb/) - Time series data storage
- [NetData](http://my-netdata.io/) - Monitor your systems and applications, the right way!
- [Speedtest](https://github.com/atribe/Speedtest-for-InfluxDB-and-Grafana) - A tool to run periodic speedtests and save them in InfluxDB for graphing in Grafana
- [Tautulli](https://tautulli.com/) - Monitor your Plex Server
- [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) - Server statistics reporting

### Note-taking and Editors

- [BulletNotes](https://gitlab.com/NickBusey/BulletNotes.git) - Note taking application
- [Trilium](https://github.com/zadam/trilium) - Build your personal knowledge base with Trilium Notes

### Password Managers

- [Bitwarden](https://bitwarden.com/) - Password and secrets manager via [bitwarden-rs](https://github.com/dani-garcia/bitwarden_rs)

### Personal Dashboards

- [Homedash](https://lamarios.github.io/Homedash2/) - Dashboard
- [Organizr](https://github.com/causefx/Organizr) - Access all your HomelabOS services in one easy place.

### Photo and Video Galleries

- [Digikam](https://www.digikam.org) - Professional Photo Management with the Power of Open Source
- [OwnPhotos](https://github.com/hooram/ownphotos) Self hosted Google Photos clone.
- [Piwigo](https://piwigo.org/) - Manage your photo collection
- [Pixelfed](https://pixelfed.org/) - A free and ethical photo sharing platform, powered by ActivityPub federation.

### Read it Later Lists

- [Wallabag](https://wallabag.org/en) - Save and classify articles. Read them later. Freely.

### Software Development

- [Code-Server](https://github.com/codercom/code-server) - Run VS Code on a remote server.
- [Drone](https://drone.io) - Drone is a self-service continuous delivery platform
- [Gitea](https://gitea.io/en-US/) - Git hosting

### Task management/To-do lists

- [Wekan](https://wekan.github.io/) - Open source Kanban board with MIT license

### VPN

- [OpenVPN](https://openvpn.net/) - A Business VPN to Access Network Resources Securely

### Web servers

- [Apache](https://httpd.apache.org/) - Web server

### Wikis

- [BookStack](https://www.bookstackapp.com/) - Simple & Free Wiki Software